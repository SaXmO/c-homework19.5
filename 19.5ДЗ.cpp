﻿#include <iostream>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "Human: Help me" << "\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Cat: Miau" << "\n";
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Dog :Woof" << "\n";
    }
};

class Dragon : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Dragon: Eating knights" << "\n";
    }
};

class Necromant : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Necromant: Life for Nerzul" << "\n";
    }
};

int main()
{
    Animal* Animals[4];
    Animals[0] = new Cat();
    Animals[1] = new Dog();
    Animals[2] = new Dragon();
    Animals[3] = new Necromant();

    for (Animal* Voices : Animals)
    {
        Voices->Voice();
    }
}